import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { NgDatepickerModule } from 'ng2-datepicker';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from '../components/app.component';

import { AuthGuard } from '../guards/auth.guard';

import { AuthentificationService, DataService } from "../services/index";

import { AppState } from "../models/index";

import { Constants } from "../constants/app.constants";

import { HomeComponent,
  NotFoundComponent,
  LoginComponent,
  RegisterComponent,
  MyPasswordsComponent,
  UnauthorizedComponent,
  SettingsComponent,
  ProfileComponent,
  HelpComponent }  from "../components/index";

/*
Declares the routes for our application
- path: the route (i.e. if path: 'home' => http://localhost.../home)
- component: the component to load for the given path
- canActivate: calls the method on the AuthGuard to check if the user can access this route

'**' => refers to pages not defined here
unauthorized => called by the AuthGuard if the user is not allowed to access a route
*/
const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'sign-in', component: LoginComponent },
  { path: 'profile/:userId', component: ProfileComponent, canActivate: [AuthGuard] }, // to change to /settings/:id
  { path: 'help', component: HelpComponent},
  { path: 'settings/:userId', component: SettingsComponent, canActivate: [AuthGuard] }, // to change to /settings/:id
  { path: 'register', component: RegisterComponent },
  // { path: 'mypwd', component: MyPasswordsComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    LoginComponent,
    RegisterComponent,
    MyPasswordsComponent,
    UnauthorizedComponent,
    HelpComponent,
    ProfileComponent,
    SettingsComponent,
  ],
  imports: [RouterModule.forRoot(
    appRoutes
    ),
    NgbModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    FormsModule,
    HttpModule,
    NgDatepickerModule
  ],
  providers: [
    AuthGuard,
    AuthentificationService,
    AppState,
    DataService,
    Constants
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
