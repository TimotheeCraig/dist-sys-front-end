import { Component , ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { Constants } from "../../constants/app.constants";

import { DataService } from '../../services/index';

import { User }  from "../../models/index";

@Component({
  templateUrl: '../../templates/settings.component.html',
})
export class SettingsComponent implements OnInit, OnDestroy {

  constructor(public toastr: ToastsManager, vcr: ViewContainerRef, private dataService: DataService, private route: ActivatedRoute, private constants: Constants) {
           this.toastr.setRootViewContainerRef(vcr);
           this.userId = route.snapshot.params['userId'];
  }

  genders = ["F", "M", "U"];

  model: User = new User("", "", "", "", 0, "");
  userId: string;

  birthdate: string;

  private userSub$: any;

  pwdConf: string = "";
  newPwd: string = "";

  ngOnInit() {
    this.loadUser();
  }

  /* Closes all subscriptions to avoid memory leaks */
  ngOnDestroy() {
    this.userSub$ ? this.userSub$.unsubscribe() : null;
  }

  /* Requests the user corresponding to the user Id in the URL to the api */
  private loadUser() : void {
    this.userSub$ = this.dataService.getSingle<User>(this.constants.userServiceAPIUrl + "/user", this.userId)
      .subscribe(user => {this.model = user;}, err => {this.toastr.error(err, "User Retrieval");})
  }

  /* Requests an update of the user's information to the api, using the id and
  the data from the form */
  submitSettings() {
    this.model.birthdate = new Date(this.birthdate).getTime();
    this.dataService.updateReturnsStatus<User>(this.constants.userServiceAPIUrl + "/user", this.userId, this.model)
      .subscribe(result => {
        if(result === true) {
          this.toastr.success("Successfully updated information", "Settings")
        }
        else {
          this.toastr.error("Error trying to update information", "Settings");
        }
    }, err => {this.toastr.error(err, "Information Update");});
  }

}
