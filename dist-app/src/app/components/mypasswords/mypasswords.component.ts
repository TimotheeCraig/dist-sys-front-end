import { Component } from '@angular/core';

@Component({
  templateUrl: '../../templates/mypasswords.component.html',
})
export class MyPasswordsComponent {

  userPin = "";

  onClickBtn(val: string) {
    if(val === 'S') {
      // Check if PIN is valid,
      // Redirect to my passwords page
    }
    else if(val === 'C') {
      this.userPin = "";
    }
    else {
      this.userPin += val;
    }
  }

}
