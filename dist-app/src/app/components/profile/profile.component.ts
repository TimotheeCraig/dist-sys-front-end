import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import { User, Procedure, PasswordCreationObject, PasswordSpecificationObject } from "../../models/index";

import { Constants } from "../../constants/app.constants";

import { DataService } from "../../services/index";

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  templateUrl: '../../templates/profile.component.html',
})
export class ProfileComponent implements OnInit, OnDestroy {
  // Route /profile/:userid -> get User detail from userid
  user: User = new User("", "", "", "", 0, "");
  userId: string;

  private userSub$: any;
  private addPwdSub$: any;
  private proceduresSub$: any;
  private passwordSub$: any;
  private passwordDeletionSub$: any;

  private canIncrease: boolean = true;

  private pageNumber: number = 1;

  procedures: Procedure[];
  pwdSpecifications: PasswordSpecificationObject[] = [];

  model: PasswordCreationObject = new PasswordCreationObject("");
  password: string = "";

  constructor(private dataService: DataService, vcr: ViewContainerRef, private route: ActivatedRoute, private toastr: ToastsManager, private constants: Constants) {
    this.toastr.setRootViewContainerRef(vcr);
    this.userId = route.snapshot.params['userId'];
  }

  ngOnInit() {
    this.loadUser();
  }

  /* Closes all subscriptions to avoid memory leaks */
  ngOnDestroy() {
    this.userSub$ ? this.userSub$.unsubscribe() : null;
    this.addPwdSub$ ? this.addPwdSub$.unsubscribe() : null;
    this.proceduresSub$ ? this.proceduresSub$.unsubscribe(): null;
    this.passwordSub$ ? this.passwordSub$.unsubscribe() : null;
    this.passwordDeletionSub$ ? this.passwordDeletionSub$.unsubscribe() : null;
  }

  /* Requests the user corresponding to the user Id in the URL to the api, loads the procedures as well */
  private loadUser() : void {
    this.userSub$ = this.dataService.getSingle<User>(this.constants.userServiceAPIUrl + "/user", this.userId)
      .subscribe(user => {this.user = user; this.loadProcedures(this.pageNumber); }, err => {this.toastr.error(err, "User Retrieval");});
  }

  /* Loads a set of procedures displayed later in a table*/
  private loadProcedures(pageNumber: number) : void {
    // /passwords/{userId}/{pageNumber}/{range}
    this.pageNumber = pageNumber;
    this.proceduresSub$ = this.dataService.getAll<Procedure>(this.constants.brokerAPIUrl + "/passwords/" + this.userId + "/" + pageNumber + "/10")
      .subscribe(res => {
        if(res.length != 0) { this.canIncrease = true; this.procedures = res; }
        else { this.canIncrease = false;
          //this.procedures = res;
          (this.pageNumber - 1) >= 1 ? this.pageNumber -= 1 : null;
        }
       }, err => { this.toastr.error(err, "Services Retrieval"); });
  }

  /* Add a service to the password generation */
  addLine() {
    // Change by calling rest
    if(this.userId) {
      this.addPwdSub$ = this.dataService.addReturnsStatus<PasswordCreationObject>(this.constants.brokerAPIUrl + "/password/" + this.userId, this.model)
        .subscribe(res => {
          if(res === true) {
            this.toastr.success("Password creation successful", "Password Creation");
            this.loadProcedures(this.pageNumber);
            this.model = new PasswordCreationObject("");
          }
          else {
            this.toastr.error("Error creating password", "Password Creation");
          }
        }, err => {this.toastr.error(err, "Password Creation");});
    }
    else {
      this.toastr.error("Error creating password", "Password Creation")
    }
  }

  increasePageNumber() {
    this.pageNumber+=1;
    this.loadProcedures(this.pageNumber);
  }

  decreasePageNumber() {
    (this.pageNumber - 1) >= 1 ? this.pageNumber -= 1 : null;
    this.loadProcedures(this.pageNumber);
  }

  /* Calls a get on the api retrieving a password from this id */
  generatePassword(id: string) {
    if(this.userId) {
      // /password/{userId}/{passwordID}
      this.passwordSub$ = this.dataService.getSingle<PasswordSpecificationObject>(this.constants.brokerAPIUrl + "/password/" + this.userId, id)
        .subscribe(res => { this.pwdSpecifications.push(res); }, err => { this.toastr.error(err, "Password Generation"); });
    }
    else {
      this.toastr.error("Error generating password", "Password Generation");
    }
  }

  /* Calls a delete on the api to delete a procedure from the id */
  deletePassword(id: string) {
    if(this.userId) {
      this.passwordDeletionSub$ = this.dataService.deleteReturnsStatus(this.constants.brokerAPIUrl + "/password/" + this.userId, id)
        .subscribe(res => {
          if(res === true) {
            // this.toastr.success("Deletion of password successful","Password Deletion");
            // this.loadProcedures(this.pageNumber);
            window.location.reload();
          }
          else {
            this.toastr.error("Deletion of password unsuccessful","Password Deletion");
          }
        }, err => {this.toastr.error(err, "Password Deletion");});
    } else {this.toastr.error("Deletion of password unsuccessful","Password Deletion");}
  }

}
