import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';

import { LoginInputObject } from "../../models/index";

import { Router } from '@angular/router';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AuthentificationService } from '../../services/index';

@Component({
  templateUrl: '../../templates/login.component.html',
})
export class LoginComponent implements OnInit, OnDestroy {

  private authSub$: any;

  model: LoginInputObject = new LoginInputObject("", "");

  constructor(public toastr: ToastsManager, vcr: ViewContainerRef, private authService: AuthentificationService, private router: Router) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  /*
  Send the login information to the auth service, and on login navigates to home
  or displays an error if there is an error.
  */
  submit(): void {
    this.authSub$ = this.authService.login(this.model.email, this.model.password)
      .subscribe(result => {
          if(result === true) {
              this.router.navigate(['/home']);
          }
          else {
            this.toastr.error("Error signing in", 'Error');
          }
      }, err => {this.toastr.error(err, "Sign-in");});
  }

  ngOnInit() {  }

  /* Closes all subscriptions to avoid memory leaks */
  ngOnDestroy() {
    this.authSub$ ? this.authSub$.unsubscribe() : null;
  }

}

/* Toastr can be found here: https://www.npmjs.com/package/ng2-toastr */
