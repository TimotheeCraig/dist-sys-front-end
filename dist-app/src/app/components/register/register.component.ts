import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { Constants } from "../../constants/app.constants";

import { Router } from '@angular/router';

import { DataService, AuthentificationService } from "../../services/index";

import { User }  from "../../models/index";

@Component({
  templateUrl: '../../templates/register.component.html',
})
export class RegisterComponent implements OnInit, OnDestroy {

  private userSub$: any;
  private authSub$: any;

  constructor(private toastr: ToastsManager,
     vcr: ViewContainerRef,
     private router: Router,
     private dataService: DataService,
     private authService: AuthentificationService,
     private constants: Constants) {
           this.toastr.setRootViewContainerRef(vcr);
  }

  genders = ["F", "M", "U"];

  model: User = new User("", "", "", "", 0, "");

  birthdate: string = "";

  pwdConf: string;


  /* Calls the api user create route, and tries to sign the user in afterwards
  if the user creation was successful */
  onSubmit(): void {
    if(this.model.password != this.pwdConf) {
      this.model.password = "";
      this.pwdConf = "";
      this.toastr.warning("Both passwords are different", "Warning");
    }
    else {
      this.model.birthdate = new Date(this.birthdate).getTime();
      this.userSub$ = this.dataService.addReturnsStatus<User>(this.constants.userServiceAPIUrl + "/users", this.model)
        .subscribe(res => {
          if(res === true) {
            this.authSub$ = this.authService.login(this.model.email, this.model.password)
              .subscribe(result => {
                  if(result === true) {
                      this.router.navigate(['/home']);
                  }
                  else {
                    this.toastr.error("Error signing in", 'Sign-in');
                  }
              }, err => this.toastr.error(err, "Sign-in"));
          }
          else {
            this.toastr.error("Couldn't Register", "Error");
          }
        }, err => this.toastr.error(err, "Registration"));

    }
  }

  ngOnInit() {

  }

  /* Closes all subscriptions to avoid memory leaks */
  ngOnDestroy() {
    this.authSub$ ? this.authSub$.unsubscribe() : null;
    this.userSub$ ? this.userSub$.unsubscribe() : null;
  }

}
