import { Component } from '@angular/core';

import { Router } from '@angular/router';

import { AuthentificationService } from "../services/index";

import { AppState } from "../models/index";

@Component({
  selector: 'app-root',
  templateUrl: '../templates/app.component.html',
})
export class AppComponent {
  isNavbarCollapsed: boolean = true;

  constructor(private authService: AuthentificationService, public appstate: AppState, private router: Router) {}

  /* Calls the lougout function on the authservice, removing the saved token
  Navigates to the home page uppon completion */
  signOut(): void {
    this.authService.logout();
    this.router.navigate(['/home']);
  }

}
