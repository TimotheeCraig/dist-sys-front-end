import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {
    var item = localStorage.getItem('currentUser');
    if(item) {
      if(route.params['userId']) {
        if(route.params['userId'] != JSON.parse(item).userId) {
              this.router.navigate(['/unauthorized']);
              return false;
        }
      }
      return true;
    }
    this.router.navigate(['/sign-in']);
    return false;
  }

}
