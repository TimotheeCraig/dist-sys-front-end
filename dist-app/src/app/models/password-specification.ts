export class PasswordSpecificationObject {
  constructor(
    public id: string,
    public serviceName: string,
    public password: string,
    public owner: string
    ) { }
}
