export class LoginInputObject {

  constructor(
    public email: string,
    public password: string
  ) {}

}
