export class LoginOutputObject {

  constructor(
    public authToken: string,
    public userId: string
  ) {}

}
