# DistApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

For this project to work properly, you must use at least [node 6.9.x](https://nodejs.org/en/) and [npm 3.x.x](https://www.npmjs.com/). To check versions, run `node -v` and `npm -v`.   

## Development server

First, navigate to the dist-app folder and run `npm install` in order to install all the require packages for the angular 2 application. This will install the packages needed for this app.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

If you wish to modify the IPs of the different services this application accesses (broker and users APIs), please modify the file `src/app/constants/app.constants.ts` and reload the project.

## Installing more packages

You can find more packages to install on [Npm's Website](https://www.npmjs.com/). Run the command `npm install <package-name> --save` to add a package. Please **do not** push any of the contents in the *node_modules* folder to the repository.
